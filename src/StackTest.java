import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;


public class StackTest {

	private Stack stack;
	
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack( 2 );
	}
	
	/** Test Empty Stack is it empty? */
	@Test
	public void newStackIsEmpty() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0,stack.size());
	}
	
	/** Test pop can we pop from empty stack. If we can't is it throw exception. */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}

	/** Test is it correct capacity of stack. */
	@Test
	public void testStackCapacity(){
		assertEquals(2,stack.capacity());
	}
	
	/** Test can it complete pushing. */
	@Test
	public void testNormalPush(){
		System.out.println(stack.size());
		stack.push('a');
		System.out.println(stack.size());
		assertEquals(1,stack.size());
		assertFalse(stack.isFull());
		assertFalse(stack.isEmpty());
		assertEquals('a',stack.peek());
	}
	
	/** Test can we push thing in full stack. If we can't, is it throw exception. */
	@Test( expected=IllegalStateException.class )
	public void testPushThingInFullStack(){
		stack.push('a');
		stack.push('b');
		stack.push('c');
	}
	
	/** Test can we push null to stack. If we can't, is it throw exception. */
	@Test( expected=IllegalArgumentException.class )
	public void testPushNull(){
		stack.push(null);
	}
	
	/** Test can we peek from empty stack. Is it null if we peek. */
	@Test
	public void testPeekInEmptyStack(){
		assertTrue(stack.isEmpty());
		assertNull(stack.peek());
	}
	
	/** Test can we pop from stack and object is removed. */
	@Test
	public void testNormalPop(){
		stack.push("ABC");
		assertSame(stack.pop(),"ABC");
		assertTrue(stack.isEmpty());
	}
	
	/** Test can we peek from stack and size isn't changed. */
	@Test
	public void testNormalPeek(){
		stack.push("Stack");
		assertSame(stack.peek(),"Stack");
		assertSame(stack.peek(),"Stack");
		assertSame(stack.peek(),"Stack");
		assertSame(stack.peek(),"Stack");
		assertFalse(stack.isEmpty());
	}
	
	/** Test is it capacity size if it has object inside stack. */
	@Test
	public void testSize(){
		stack.push('a');
		assertEquals(stack.size(),1);
	}
	
	/** Test is it correct capacity. */
	@Test
	public void testCapacity(){
		assertEquals(stack.capacity(),2);
	}
	
}
